/*
 * MIT License
 *
 * Copyright (c) 2021 Unbound Software Development Svenska AB
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package main

import (
	"fmt"
	"os"
	"sync"
	"sync/atomic"
	"time"

	"github.com/alecthomas/kong"
	"github.com/hashicorp/go-hclog"
	"github.com/hashicorp/go-plugin"

	"gitlab.com/unboundsoftware/sloth/model"
	mp "gitlab.com/unboundsoftware/sloth/model/plugin"
)

type Config struct {
	InMemory bool `name:"inmemory" env:"INMEMORY" help:"Use in-memory store"`
}

type InMemory struct {
	requests   sync.Map
	lastId     uint64
	timeSource func() time.Time
	quit       chan bool
	logger     hclog.Logger
	mutex      sync.Mutex
	started    bool
}

func (i *InMemory) Configure(args []string) (bool, error) {
	cli := &Config{}
	cmd, err := kong.New(
		cli,
		kong.Description("inmemory_store is a pluggable store for sloth"),
		kong.Writers(os.Stdout, os.Stdin),
	)
	if err != nil {
		return false, err
	}
	_, err = cmd.Parse(args)
	if err != nil {
		return false, err
	}

	if !cli.InMemory {
		return false, nil
	}

	i.timeSource = time.Now
	i.quit = make(chan bool)

	return true, nil
}

func (i *InMemory) IsSource() bool {
	return false
}

func (i *InMemory) IsSink() bool {
	return false
}

func (i *InMemory) IsStore() bool {
	return true
}

func (i *InMemory) Start(handler model.Handler, quitter model.Quitter) error {
	i.mutex.Lock()
	defer i.mutex.Unlock()
	ticker := time.NewTicker(time.Second)
	quit := make(chan error)
	go func() {
		for {
			select {
			case <-i.quit:
				return
			case e := <-quit:
				quitter.Quit(e)
			case <-ticker.C:
				now := i.timeSource()
				i.requests.Range(i.processStoredRequest(now, handler.Handle, quit))
				time.Sleep(time.Second)
			}
		}
	}()
	i.started = true
	return nil
}

func (i *InMemory) processStoredRequest(now time.Time, handler func(model.Request) error, quit chan error) func(key, value interface{}) bool {
	return func(key, value interface{}) bool {
		s := value.(stored)
		if !s.Req.DelayedUntil.After(now) {
			if err := handler(s.Req); err == nil {
				_ = s.Ack()
			} else {
				i.logger.Error("error handling request", "error", err)
				quit <- err
			}
		}
		return true
	}
}

func (i *InMemory) Stop() error {
	i.mutex.Lock()
	defer i.mutex.Unlock()
	i.logger.Info("stopping")
	if i.started {
		i.quit <- true
	}
	return nil
}

func (i *InMemory) Add(request model.Request) error {
	id := atomic.AddUint64(&i.lastId, 1)
	stringId := fmt.Sprintf("%d", id)
	_, _ = i.requests.LoadOrStore(stringId, stored{
		store: i,
		Id:    stringId,
		Req:   request,
	})
	return nil
}

var _ model.Store = &InMemory{}

type stored struct {
	store *InMemory
	Id    string
	Req   model.Request
}

func (s stored) Ack() error {
	s.store.requests.Delete(s.Id)
	return nil
}

func main() {
	logger := hclog.Default()
	plugin.Serve(&plugin.ServeConfig{
		HandshakeConfig: mp.Handshake,
		Plugins: map[string]plugin.Plugin{
			"store": &mp.StorePlugin{
				Impl: &InMemory{logger: logger},
			},
		},
		GRPCServer: plugin.DefaultGRPCServer,
	})
}
