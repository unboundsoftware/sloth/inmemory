/*
 * MIT License
 *
 * Copyright (c) 2021 Unbound Software Development Svenska AB
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package main

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"reflect"
	"strings"
	"testing"
	"time"

	"github.com/hashicorp/go-hclog"
	"github.com/sanity-io/litter"
	"github.com/stretchr/testify/assert"

	"gitlab.com/unboundsoftware/sloth/model"
)

func TestInMemory_Start(t *testing.T) {
	type args struct {
		handler model.Handler
	}
	tests := []struct {
		name       string
		args       args
		wantErr    bool
		wantLogged []string
	}{
		{
			name: "no requests",
			args: args{
				handler: model.HandlerFunc(func(request model.Request) error {
					return nil
				}),
			},
			wantErr:    false,
			wantLogged: []string{"[INFO]  stopping\n"},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			logged := &bytes.Buffer{}
			logger := hclog.New(&hclog.LoggerOptions{Output: logged, DisableTime: true})
			quitter := make(model.ChannelQuitter, 10)
			i := &InMemory{
				timeSource: func() time.Time {
					return time.Date(2021, 1, 25, 15, 55, 12, 123456789, time.UTC)
				},
				quit:   make(chan bool, 10),
				logger: logger,
			}
			if err := i.Start(tt.args.handler, quitter); (err != nil) != tt.wantErr {
				t.Errorf("Start() error = %v, wantErr %v", err, tt.wantErr)
			}
			time.Sleep(time.Second)
			_ = i.Stop()
			time.Sleep(time.Second)
			assert.Equal(t, strings.Join(tt.wantLogged, "\n"), logged.String())
		})
	}
}

func TestInMemory_Add(t *testing.T) {
	type args struct {
		request model.Request
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "add request",
			args: args{request: model.Request{
				DelayedUntil: time.Date(2021, 1, 25, 15, 55, 12, 123456789, time.UTC),
				Target:       "test://host/path",
				Payload:      json.RawMessage("abc"),
			}},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			i := &InMemory{}
			if err := i.Add(tt.args.request); (err != nil) != tt.wantErr {
				t.Errorf("Add() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestInMemory_processStoredRequest(t *testing.T) {
	type args struct {
		handler func(model.Request) error
		value   stored
	}
	tests := []struct {
		name       string
		args       args
		want       bool
		wantLogged []string
	}{
		{
			name: "request in future",
			args: args{
				value: stored{
					Id:  "a",
					Req: model.Request{DelayedUntil: time.Date(2021, 1, 25, 16, 0, 0, 0, time.UTC)},
				},
			},
			want:       true,
			wantLogged: []string{},
		},
		{
			name: "error handling request",
			args: args{
				handler: func(request model.Request) error {
					want := model.Request{DelayedUntil: time.Date(2021, 1, 25, 15, 55, 0, 0, time.UTC)}
					if !reflect.DeepEqual(request, want) {
						t.Errorf("Start() got %s, want %s", litter.Sdump(request), litter.Sdump(want))
					}
					return errors.New("error")
				},
				value: stored{
					Id:  "a",
					Req: model.Request{DelayedUntil: time.Date(2021, 1, 25, 15, 55, 0, 0, time.UTC)},
				},
			},
			want: true,
			wantLogged: []string{
				"[ERROR] error handling request: error=error\n",
			},
		},
		{
			name: "success",
			args: args{
				handler: func(request model.Request) error {
					return nil
				},
				value: stored{
					Id:  "a",
					Req: model.Request{DelayedUntil: time.Date(2021, 1, 25, 15, 55, 0, 0, time.UTC)},
				},
			},
			want:       true,
			wantLogged: []string{},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			logged := &bytes.Buffer{}
			logger := hclog.New(&hclog.LoggerOptions{Output: logged, DisableTime: true})
			i := &InMemory{
				logger: logger,
			}
			quit := make(chan error, 10)
			now := time.Date(2021, 1, 25, 15, 55, 12, 123456789, time.UTC)
			tt.args.value.store = i
			if got := i.processStoredRequest(now, tt.args.handler, quit)("a", tt.args.value); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("processStoredRequest() = %v, want %v", got, tt.want)
			}
			assert.Equal(t, strings.Join(tt.wantLogged, "\n"), logged.String())
		})
	}
}

func TestInMemory_Configure(t *testing.T) {
	tests := []struct {
		name       string
		args       []string
		want       bool
		wantErr    assert.ErrorAssertionFunc
		wantLogged []string
	}{
		{
			name: "invalid parameter",
			args: []string{"--invalid"},
			want: false,
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.EqualError(t, err, "unknown flag --invalid")
			},
			wantLogged: nil,
		},
		{
			name:       "no parameters",
			args:       []string{},
			want:       false,
			wantErr:    assert.NoError,
			wantLogged: nil,
		},
		{
			name:       "success",
			args:       []string{"--inmemory"},
			want:       true,
			wantErr:    assert.NoError,
			wantLogged: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			logged := &bytes.Buffer{}
			logger := hclog.New(&hclog.LoggerOptions{Output: logged, DisableTime: true})
			i := &InMemory{
				logger: logger,
			}
			got, err := i.Configure(tt.args)
			if !tt.wantErr(t, err, fmt.Sprintf("Configure(%v)", tt.args)) {
				return
			}
			assert.Equalf(t, tt.want, got, "Configure(%v)", tt.args)
			assert.Equal(t, strings.Join(tt.wantLogged, "\n"), logged.String())
		})
	}
}

func TestInMemory_IsSource(t *testing.T) {
	assert.Equalf(t, false, (&InMemory{}).IsSource(), "IsSource()")
}

func TestInMemory_IsSink(t *testing.T) {
	assert.Equalf(t, false, (&InMemory{}).IsSink(), "IsSink()")
}

func TestInMemory_IsStore(t *testing.T) {
	assert.Equalf(t, true, (&InMemory{}).IsStore(), "IsStore()")
}
